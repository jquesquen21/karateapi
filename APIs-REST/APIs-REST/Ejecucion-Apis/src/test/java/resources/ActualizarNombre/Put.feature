Feature: Actualizar el nombre de la mascota y el estatus de la mascota a "sold"

  Background:
    Given url urlDemo
    * def body =
    """
    {
    "id": 1,
      "category": {
        "id": 1,
        "name": "Pastor"
      },
    "name": "Snoopy",
    "photoUrls": [
      "string"
      ],
    "tags": [
      {
        "id": 1,
        "name": "Snoopy"
      }
    ],
    "status": "activo"
    }
    """

  @DermoApis
  Scenario Outline: Actualizar nombre y estatus de la mascota
    Given path 'pet'
    And header Content-Type = 'application/json'
    * set body.category.name = "<cstring>"
    * set body.status = "<cvariable>"
    And request body
    When method PUT
    Then status 200
    * print response

    Examples:
      | cstring | cvariable |
      | Aleman  | sold      |
