package resources;

import com.intuit.karate.junit5.Karate;
import com.intuit.karate.Results;
import com.intuit.karate.Runner;


public class KarateRunner {

    @Karate.Test
    Karate runTest() {
        return Karate.run("classpath:resources/ActualizarNombre","classpath:resources/AgregarMascota","classpath:resources/BusquedaStatus")
                .karateEnv("cert")
                .tags("@DermoApis");

    }

}
