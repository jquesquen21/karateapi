# KarateAPI

## Getting started

Este proyecto contine feature de pruebas automatizadas utilizando el framework Karate 
para probar APIs REST debemos ingresar al GitLab y descargarlo.

## Prerequisites:

- Java JDK 8 o superior instalado
- Maven instalado para la gestión de dependency

## Configuration and Execution:

- Clonar el proyecto desde el repositorio:
```
    - Ingrese desde tu navegador preferido a la siguiente url: https://gitlab.com/jquesquen21/karateapi.git
    - Verifique estar posicionado en el repositorio del proyecto (main)
    - Da clic en el boton clone y copia su url
    - Create un archivo nuevo desde tu disco local c:
    - Desde tu explorador de archivo creado, abre el terminar Git 
    - Ejecute el siguiente comando: git clone https://gitlab.com/jquesquen21/proyectoweb.git
```
- Importar el proyecto clonado a IntelliJ IDEA
```
    - Abre el IntelliJ IDEA
    - Clic en Open
    - Dirígete hacia tu proyecto clonado y selecionalo 
    - Da clic en la opcion OK
```

## Execute the Karate test cases

```
 - Ejecuta la class KarateRunner, que se encuentra en la siguiente ruta: 'src/java/resources/KarateRunner.class'
 - Desde el Terminar tambien puedes ejecutar el siguiente comando: >mvn test -Dkarate.options="--tags @DermoApis" -Dtest=KarateRunner"
```

## Project Structure

```
    - 'src/java/req': Contiene un json de registro de datos general
    - 'src/java/resurces': Contiene los casos de pruebas creados con Karate en archivos feature.
    - 'src/java/resurces': Contiene la class runnertest para la ejecución de los feature.
    - 'src/java/karate-config.js': Contiene la base url que se consume para la ejecución de las apis.
    -  pom.xml :  Archivo de configuración de Maven
```







