Feature: Añadir una mascota a la tienda

  Background:
    Given url urlDemo
    * def req = read('classpath:req/IngresaData.json')

  @DermoApis
  Scenario Outline: Añadir una nueva mascota
    Given path 'pet'
    And header Content-Type = 'application/json'
    * set req.id = <id>
    * set req.category.id = <ids>
    * set req.category.name = "<cstring>"
    * set req.name = "<cname>"
    * set req.photoUrls[0] = "<cphotoUrls>"
    * set req.tags[0].id = <idc>
    * set req.tags[0].name = "<name>"
    * set req.status = "<cvariable>"
    And request req
    When method POST
    Then status 200
    * print response

    Examples:
      | id | ids | cstring   | cname  | cphotoUrls | idc | name   | cvariable |
      | 1  | 1   | Pastor    | Snoopy | string     | 1   | Snoopy | activo    |
      | 2  | 2   | Schnauzer | Toto   | string     | 2   | Toto   | activo    |


