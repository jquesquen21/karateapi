Feature: Consultar la mascota ingresada previamente (Búsqueda por ID)

  Background:
    Given url urlDemo

  @GET
  Scenario Outline: Consultar mascota por Id
    Given path "pet/" + <id>
    When method GET
    Then status 200
    And print response

    Examples:
      | id |
      | 1  |
      | 2  |


